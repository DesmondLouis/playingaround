// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAkK0MWUJWz5QT-76d8HeHiH7qmJe5M1KE",
    authDomain: "hackathon-ae316.firebaseapp.com",
    databaseURL: "https://hackathon-ae316.firebaseio.com",
    projectId: "hackathon-ae316",
    storageBucket: "hackathon-ae316.appspot.com",
    messagingSenderId: "1042919422572",
    appId: "1:1042919422572:web:cec9d9d16e42afdc"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
