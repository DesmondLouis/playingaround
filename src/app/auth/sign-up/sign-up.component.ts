import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  public userName;
  public userPassword;
  constructor(private authService: AuthService) { }

  ngOnInit() {
  }
  public signUp() {
    this.authService.SignIn(this.userName, this.userPassword);
  }

  public googleAuth() {
    this.authService.GoogleAuth();
  }

}
