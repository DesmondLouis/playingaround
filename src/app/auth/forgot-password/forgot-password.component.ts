import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  public passwordResetEmail;
  constructor(private authService: AuthService) { }

  ngOnInit() {
  }
  public onPasswordResetEmail() {
    this.authService.ForgotPassword(this.passwordResetEmail);
  }

}
