import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  public userName;
  public userPassword;
  constructor(private authService: AuthService) { }

  ngOnInit() {
  }
  public signIn() {
    this.authService.SignIn(this.userName, this.userPassword);
  }

  public googleAuth() {
    this.authService.GoogleAuth();
  }
}
